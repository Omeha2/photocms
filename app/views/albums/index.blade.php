@extends('layouts.scaffold')

@section('main')

<h1>Own Albums</h1>

<p>{{ link_to_route('albums.create', 'Add new album',[],['class'=>'btn btn-info']) }}
{{ link_to_action('AlbumsController@shared', 'Shared albums',[],['class'=>'btn btn-info']) }}</p>

@if ($albums->count())
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>id</th>
				<th>Uid</th>
				<th>Name</th>
				<th>Status</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($albums as $album)
				<tr>
					<td>{{{ $album->id }}}</td>
					<td>{{{ $album->uid }}}</td>
					<td>{{ link_to_action('PhotosController@index', $album->name, array($album->id), array('class' => '')) }} </td>
					<td>{{{ $album->status }}}</td>
					@if($album->pivot->access >= 2)
                    <td>{{ link_to_route('albums.edit', 'Edit', array($album->id), array('class' => 'btn btn-info')) }}</td>
                    @endif
                    @if($album->pivot->access == 3)
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('albums.destroy', $album->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
                    @endif
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no albums
@endif

@stop
