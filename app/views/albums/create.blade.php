@extends('layouts.scaffold')

@section('main')

<h1>Create Album</h1>

{{ Form::open(array('route' => 'albums.store')) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>

        <li>
            {{ Form::label('status', 'Public?') }}
            {{--{{ Form::input('number', 'status') }}--}}
{{--            {{ Form::checkbox('status', 'true',  true, array('class' => 'name')); }}--}}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
		</li>
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


