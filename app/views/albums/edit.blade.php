@extends('layouts.scaffold')

@section('main')
<script>
  jQuery(function () {
     function split( val ) {
        return val.split( /,\s*/ );
      }
      function extractLast( term ) {
        return split( term ).pop();
      }
    jQuery('#user_id').autocomplete({
    source: function( request, response ) {
                    var term = request.term;


                    $.getJSON( "/user_list", {value: request.term}, function( data, status, xhr ) {
                    response({'tets':'sdf','sdffds':'fffd'});
//                        response( data );
console.log(data);
                    });
                    }});
                    });
</script>
<h1>Редагування альбому</h1>
<h3>Назва альбому</h3>
{{ Form::model($album, array('method' => 'PATCH', 'route' => array('albums.update', $album->id))) }}
	<ul>
        <li>
            {{ Form::label('name', 'Name:') }}
            {{ Form::text('name') }}
        </li>
		<li>
			{{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
		</li>
	</ul>
	{{ Form::close() }}

<h3>Доступ до альбому</h3>
	{{ Form::open(array('method' => 'POST', 'action' => array('AlbumsController@giveAccess', $album->id))) }}
	<ul>
	  <li>
	  {{ Form::label('user_id', 'ID користувача',['class'=>'control-label']) }}
	  {{ Form::text('user_id') }}
	  <p class="help-block">If user doesn't exist he will be created</p>
	  </li>
	  <li>
	  {{ Form::label('user_access', 'Права') }}
	  {{ Form::select('user_access', array('2' => 'Редагування', '1' => 'Читання'));}}
	  </li>
	  <li>
	  {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}
	  </li>
	</ul>
{{ Form::close() }}

    <h3>Кому відкритий альбом</h3>
	@if ($users->count())
    	<table class="table table-striped table-bordered" style="width: 60%">
    		<thead>
    			<tr>
    				<th width="75">ID</th>
    				<th>Імя</th>
    				<th>Права</th>
    				<th width="70">Дії</th>
    			</tr>
    		</thead>

    		<tbody>
    			@foreach ($users as $user)
    				<tr>
    					<td>{{{ $user->id }}}</td>
    					<td>{{{ $user->login }}}</td>
    					@if($user->pivot->access == 1)
    					<td>Читання</td>
    					@elseif($user->pivot->access == 2)
    					<td>Редагування</td>
    					@elseif($user->pivot->access == 3)
    					<td>Власник</td>
    					@else
    					<td>WTF? {{{$user->pivot->access}}}</td>
    					@endif
    					<td>
    					   {{ Form::open(array('method' => 'DELETE', 'action' => array('AlbumsController@deleteAccess', $album->id))) }}
    					   {{ Form::hidden('user_id',$user->id) }}
                              {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                           {{ Form::close() }}</td>
    				</tr>
    			@endforeach
    		</tbody>
    	</table>
    @else
    	There are no access
    @endif


@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop
