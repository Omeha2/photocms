@extends('layouts.scaffold')

@section('main')

<h1>Create Photo</h1>
{{ action('PhotosController@store',$album_id) }}
{{ Form::open(array('action' => array('PhotosController@store',$album_id),'files'=>true,'id'=>'form1')) }}
	<ul>

        <li>

            {{ Form::label('src', 'Src:') }}
            {{ Form::file('src',['id'=>'fupload','multiple']) }}
        </li>

		<li>
			{{ Form::submit('Submit', array('class' => 'btn btn-info','id'=>'submit1')) }}
		</li>
	</ul>
{{ Form::close() }}
<progress id="bvb_progress"></progress>
<div id="upl_info"></div>
<div id="preview"></div>
@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
<!-- The Templates plugin is included to render the upload/download listings -->
<script src="http://blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- blueimp Gallery script -->
<script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script>

    jQuery("body").on('change', '#fupload', function (ee) {
    document.cc = 0;
    ee.preventDefault();
        document.uplfiles = document.getElementById('fupload').files;
        for(var k=0;k<document.uplfiles.length;k++){
          /*PREVIEW*/
          var fr = new FileReader();

          fr.onload = function(e) {
             jQuery('<img>', {
              src: e.target.result,
              width: '300px',
              data_img_id: document.cc
             }).appendTo('#preview');
             document.cc++;
          }
          fr.readAsDataURL(document.uplfiles[k]);
          console.log(jQuery('#preview img').length);
        }
    });
function upl(files,k){
if(k>=files.length)
  return '';
          var fd = new FormData();
          fd.append('src',files[k]);
                  jQuery.ajax({
                      url: '{{ action('PhotosController@store',$album_id) }}', //Server script to process data
                      type: 'POST',
                      xhr: function () {  // Custom XMLHttpRequest
                          var myXhr = jQuery.ajaxSettings.xhr();
                          if (myXhr.upload) { // Check if upload property exists
                              myXhr.upload.addEventListener('progress', progressHandlingFunction, false); // For handling the progress of the upload
                          }
                          return myXhr;
                      },
                      //Ajax events
                      beforeSend: function () {
                      },
                      success: function (e, e1) {
                      },
                      error: function () {
                          console.log('error upload');
                      },
                      complete: function(){
                          console.log('UPLOAD DONE');
                          upl(files,k+1);
                      },
                      // Form data
                      data: fd,
                      mimeType: 'multipart/form-data',
                      //Options to tell jQuery not to process data or worry about content-type.
                      cache: false,
                      contentType: false,
                      processData: false
                  });
}
    jQuery("body").on('click', '#submit1', function (ee) {
    ee.preventDefault();
        var files = document.uplfiles;
        upl(files,0);
    });

    function progressHandlingFunction(e) {
        if (e.lengthComputable) {
            jQuery('#bvb_progress').attr({value: e.loaded, max: e.total});
            console.log({value: e.loaded, max: e.total});
        }
    }
</script>

@stop


