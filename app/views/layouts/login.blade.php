<!DOCTYPE html>
<html>
<head>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
<body>
Login by VK<br>
<div id="vk_auth"></div>
<div id="vk_auth11" style="display: block">

 {{ Form::open(['route'=>'session.store'])}}
   {{ Form::text('uid',null,['class'=>'form-control','placeholder'=>'Login','autofocus'])}}
   {{ Form::text('hash',null,['class'=>'form-control','placeholder'=>'Password'])}}
   {{ Form::hidden('last_name')}}
   {{ Form::hidden('first_name')}}
   {{ Form::hidden('photo')}}
   {{ Form::submit('Login',['class'=>'btn btn-lg btn-success btn-block'])}}
 {{ Form::close()}}
</div>
<script type="text/javascript" src="http://vk.com/js/api/openapi.js?105"></script>
<script>
VK.init({apiId: '4620032' });
VK.Widgets.Auth("vk_auth", {width: "200px", onAuth: function(data) {
	console.log(data);
	jQuery('[name="uid"]').val(data.uid);
	jQuery('[name="hash"]').val(data.hash);
	jQuery('[name="last_name"]').val(data.last_name);
	jQuery('[name="first_name"]').val(data.first_name);
	jQuery('[name="photo"]').val(data.photo);
	jQuery('form').submit();
}});
</script>

</body>
</html>