<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotAlbumUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('album_user', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('album_id')->unsigned()->index();
			$table->string('user_id')->index();
			$table->foreign('album_id')->references('id')->on('albums')->onDelete('cascade');
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->string('access');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('album_user');
	}

}
