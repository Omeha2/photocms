<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Event::listen('illuminate.query', function($sql)
{
//	print_r($sql);
//	print_r('<br>');
});


Route::get('login', 'SessionController@create');
Route::get('login/{id}', 'SessionController@in');
Route::get('logout', 'SessionController@destroy');

Route::resource('session', 'SessionController', ['only' => ['store', 'destroy', 'create']]);

Route::group(['before' => 'auth'], function () {
    Route::resource('albums', 'AlbumsController',['except'=>['show']]);
	Route::get('albums/shared', 'AlbumsController@shared');
	Route::get('albums/{album_id}', 'PhotosController@index');
	Route::get('albums/{album_id}/create', 'PhotosController@create');
	Route::post('albums/{album_id}', 'PhotosController@store');
	Route::delete('albums/{album_id}/access', 'AlbumsController@deleteAccess');
	Route::post('albums/{album_id}/access', 'AlbumsController@giveAccess');
	Route::get('albums/{album_id}/{id}/', 'PhotosController@show');
	Route::get('albums/{album_id}/{id}/edit', 'PhotosController@edit');
	Route::post('albums/{album_id}/{id}/move', 'PhotosController@move');
	Route::delete('albums/{album_id}/{id}', 'PhotosController@destroy');

	Route::get('user_list', 'UsersController@user_list');

	Route::get('/', function () {
		return View::make('hello');
	});
});

Route::group(['before' => 'auth'], function () {

});


Route::resource('users', 'UsersController');



