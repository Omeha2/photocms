<?php

class Photo extends Eloquent
{
    protected $guarded = array();

    public function album()
    {
        return $this->belongsTo('Album');
    }

    public static $rules = array(
        'src' => 'required|image'
    );

    public function delete()
    {
        @unlink(public_path() . '/files/' . $this->album_id.'/'.$this->src);
        @unlink(public_path() . '/files/' . $this->album_id.'/thumb_'.$this->src);
//        unlink($this->thumb_src);
        return parent::delete();
    }
}
