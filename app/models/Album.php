<?php

class Album extends Eloquent {
	protected $guarded = array();

	public function photos(){
		return $this->hasMany('Photo');
	}

	public function Author(){
		dd($this);
		dd($this->belongsToMany('User')->where('user_id','=',Auth::user()->id));
		return $this->belongsToMany('User')->where('user_id','=',Auth::user()->id)->withPivot('access')->first();
	}

	public function users(){
		return $this->belongsToMany('User')->withPivot('access');
	}


	public static $rules = array(
		'name' => 'required',
	);
}
