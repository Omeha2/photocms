<?php

class AlbumsController extends BaseController
{

    /**
     * Album Repository
     *
     * @var Album
     */
    protected $album;

    public function __construct(Album $album)
    {
        $this->album = $album;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // !!!!! Replace this two lines
//        $albums = $this->album->get();
        $albums = Auth::user()->ownAlbums;

        return View::make('albums.index', compact('albums'));
    }

    public function shared()
    {
        // !!!!! Replace this two lines
//        $albums = $this->album->get();
        $albums = Auth::user()->shared;

        return View::make('albums.shared', compact('albums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('albums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $validation = Validator::make($input, Album::$rules);

        if ($validation->passes()) {
            $a = $this->album->create($input);

            File::makeDirectory(public_path() . '/files/'.$a->id.'/', 0775, true, true);

            Auth::user()->ownAlbums()->attach($a,['access'=>'3']);
            return Redirect::route('albums.index');
        }

        return Redirect::route('albums.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        $album = Album::where('id', '=', $id)->first();
        if (is_null($album)) {
            return 'Album not exist';
//            return Redirect::route('albums.index');
        }

        $creator = $album->users()->where('user_id', '=', Auth::user()->id)->first();
        if (is_null($creator) || $creator->pivot->access <= 1) {
            return 'You can\'t edit this album';
        }

        $users = $album ->users()->get();

        return View::make('albums.edit', compact('album','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, Album::$rules);

        if ($validation->passes()) {
            $album = $this->album->find($id);

            $creator = $album ->users()->where('user_id','=',Auth::user()->id)->first();
            if($creator->pivot->access<=1){
                return 'You can\'t edit this album';
            }
            $album->update($input);

            return Redirect::route('albums.index');
        }

        return Redirect::route('albums.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    public function giveAccess($album_id){
        $album = Album::where('id', '=', $album_id)->first();
        if (is_null($album)) {
            return 'Album not exist';
        }

        $creator = $album->users()->where('user_id', '=', Auth::user()->id)->first();
        if (is_null($creator) || $creator->pivot->access <= 2) {
            return 'You can\'t change access to this album';
        }

        $user_id = Input::get('user_id');
        $user = User::where('id','=',$user_id)->first();

        // if user is now exist, create them
        if(!isset($user)){
            $user = User::create([
                'id' => $user_id,
                'password' => '',
                // To-do: get name from VK by id
                'login' => str_random(8),
            ]);
        }

        $user_access = Input::get('user_access');
        if($user_access == '3')
            return 'You can\'t change album owner';
        // Delete access if user already have it
        $album->users()->detach($user_id);
        $album->users()->attach($user_id,['access'=>$user_access]);

        return 'Set access success to user: '.$user_id.' name: '.$user->login;
    }
    public function deleteAccess($album_id){
        $album = Album::where('id', '=', $album_id)->first();
        if (is_null($album)) {
            return 'Album not exist';
        }

        $user_id = Input::get('user_id');

        if($user_id == Auth::user()->id){
            return 'You cnt delete owner from album';
        }

        $creator = $album->users()->where('user_id', '=', Auth::user()->id)->first();
        if (is_null($creator) || $creator->pivot->access <= 2) {
            return 'You can\'t change access to this album';
        }

        $album->users()->detach($user_id);
        return 'Delete access success';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $album = $this->album->find($id);
        $creator = $album ->users()->where('user_id','=',Auth::user()->id)->first();
        if($creator->pivot->access<=2){
            return 'You can\'t delete this album';
        }
        $album->photos()->delete();
        File::deleteDirectory(public_path() . '/files/'.$album->id.'/');
        $album->delete();


        return Redirect::route('albums.index');
    }

}
