<?php

class SessionController extends BaseController
{

    public function create()
    {
        if (Auth::guest()) {
            return View::make('session.login');
        } else {
            return 'you login ';
        }
    }

    public function in($id){
        $user = User::where('id', '=', $id)->first();

        if(!isset($user)){
            return 'User not exist';
        }
        Auth::logout();
        Auth::login($user);
    }

    public function store()
    {
        if (Auth::guest()) {
            $input = Input::all();

            $s = ('4620032' . $input['uid'] . 'SYkai1N3XMNgh44aQpGq');

			if ($input['hash'] === md5($s)) {
				$user = User::where('id', '=', $input['uid'])->first();

				if(!isset($user)){
					$user = User::create([
						'id' => $input['uid'],
						'password' => '',
						'first_name' => $input['first_name'],
						'last_name' => $input['last_name'],
						'photo_src' => $input['photo'],
						'login' => str_random(8),
					]);
				}


                Auth::login($user);
				return Redirect::intended('/')->with('message', 'You have been login!');
            } else {
		   	    return 'wrong hash';
            }

            return Redirect::back()->with('message', 'Wrong login or password');


        } else {
			dd('already LOGIN');
//            return Redirect::to('user/profile');
        }
    }

    public function destroy()
    {
        Auth::logout();
        return Redirect::intended('/')->with('message', 'You have been logout Ага =)');
    }

}
